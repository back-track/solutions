public class NQueensProblem {
    private int[][] board;
    private int numOfQueens;

    public NQueensProblem(int numOfQueens) {
        this.numOfQueens = numOfQueens;
        this.board = new int[numOfQueens][numOfQueens];
    }

    public void solve() {
        if (setQueens(0)) {
            printQueens();
        } else {
            System.out.println("there is no solution...");
        }
    }

    private boolean setQueens(int colIndex) {
        if (colIndex == numOfQueens) {
            return true;
        }

        for (int rowIndex = 0; rowIndex < numOfQueens; ++rowIndex ) {
            if (isPlaceValid(rowIndex, colIndex)) {
                board[rowIndex][colIndex] = 1;

                if (setQueens(colIndex +1)) {
                    return true;
                }

                //BACKTRAFKICG!
                board[rowIndex][colIndex] = 0;
            }
        }
        return false;
    }

    private boolean isPlaceValid(int rowIndex, int colIndex) {


        //checking row
        for (int i = 0; i < colIndex; i++) {
            if (board[rowIndex][i] == 1) {
                return false;
            }
        }

        //checking first diagonal
        for (int i = rowIndex, j = colIndex; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == 1) {
                return false;
            }
        }

        //checking second diagonal
        for (int i = rowIndex, j = colIndex; i < board.length && j >= 0; i++, j--) {
            if (board[i][j] == 1) {
                return false;
            }
        }

        return true;
    }

    private void printQueens() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == 1) {
                    System.out.print  (" x ");
                } else {
                    System.out.print(" - ");
                }
            }
            System.out.println();
        }
    }
}
